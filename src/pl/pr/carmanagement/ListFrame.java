package pl.pr.carmanagement;

import javax.swing.JFrame;
import javax.swing.JList;

public class ListFrame extends JFrame
{
	private void setFramesParametres()
	{
		setVisible(true);
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	private void addJList()
	{
		String array[] = new String[Car.listOfCars.size()];
		int i=0;
		for(Car c : Car.listOfCars)
		{
			array[i++]=c.toString();
		}
		//GOWNO
		//NIEWYROBIE
		//NOJANIEMOGE
		JList listInFrame = new JList(array);
		add(listInFrame);
	}
	
	public ListFrame()
	{
		super("LIST");
		setFramesParametres();
		addJList();
	}

}
