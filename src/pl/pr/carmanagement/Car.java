package pl.pr.carmanagement;


import java.util.ArrayList;
import java.util.List;

public class Car {
	private String brand;
	private String model;
	private double prise;
	private int year;
	
	public static List<Car> listOfCars = new ArrayList<Car>();
	
	public Car(String brand, String model, double prise, int year){
		setBrand(brand);
		setModel(model);
		setPrise(prise);
		setYear(year);
		listOfCars.add(this);
		// konstrutor
	}
	
	

	public String getBrand() {
		return brand;
	}
	

	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public double getPrise() {
		return prise;
	}
	public void setPrise(double prise) {
		this.prise = prise;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	@Override 
	public String toString () {
		return brand+" " +model+" "+prise+" "+year;
		
	}
	
	
	
	
	
	
	
}
